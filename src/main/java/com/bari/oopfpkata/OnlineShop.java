package com.bari.oopfpkata;

class OnlineShop {

    private ProductQuantityUpdatePort productQuantityUpdatePort;

    private Basket basket;

    private CustomerAddress customerAddress;


    public OnlineShop(ProductQuantityUpdatePort productQuantityUpdatePort) {
        this.productQuantityUpdatePort = productQuantityUpdatePort;
        this.basket = new Basket(productQuantityUpdatePort);
    }

    public void addItem(ShopProduct product, Integer amount) {
        basket.addItem(product, amount);
    }

    public void addCustomerAddress(CustomerAddress customerAddress) {
        this.customerAddress = customerAddress;
    }

    public Order order() {
        if (basket == null || customerAddress == null) {
            throw new IllegalStateException("basket == null || customerAddress == null");
        }
        Order order = basket.order(customerAddress);
        order.submit(productQuantityUpdatePort);
        return order;
    }
}
