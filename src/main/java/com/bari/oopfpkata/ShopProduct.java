package com.bari.oopfpkata;

public class ShopProduct {
    private String name;
    private String isdn;
    private Integer price;

    public ShopProduct(String name, String isdn, Integer price) {
        this.name = name;
        this.isdn = isdn;
        this.price = price;
    }

    public Integer getPrice() {
        return price;
    }
}
