package com.bari.oopfpkata;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ProductCatalog implements ProductQuantityUpdatePort {

    private Map<String, ShopProduct> productNamesWithProducts = new HashMap<>();

    private Map<ShopProduct, Integer> productsWithQuantities = new HashMap<>();


    @Override
    public void decreaseProductQuantities(ShopProduct product, Integer amount) {
        productsWithQuantities.put(product, productsWithQuantities.get(product) - amount);
        if (productsWithQuantities.get(product) < 0) {
            productsWithQuantities.put(product, 0);
        }
    }

    public Optional<ShopProduct> findProductByName(String name) {
        return Optional.ofNullable(productNamesWithProducts.get(name));
    }

    public boolean checkAvailabilityOfProduct(ShopProduct product) {
        return productsWithQuantities.containsKey(product);
    }
}
