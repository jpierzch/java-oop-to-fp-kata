package com.bari.oopfpkata;

interface ProductQuantityUpdatePort {

    void decreaseProductQuantities(ShopProduct product, Integer amount);
}
