package com.bari.oopfpkata;

import java.util.HashMap;
import java.util.Map;

public class Basket {

    private ProductQuantityUpdatePort productCatalogPort;

    private Map<ShopProduct, Integer> itemsWithQuantities = new HashMap<>();


    public Basket(ProductQuantityUpdatePort productCatalogPort) {
        this.productCatalogPort = productCatalogPort;
    }

    public void addItem(ShopProduct product, Integer amount) {
        if (itemsWithQuantities.containsKey(product) == false) {
            itemsWithQuantities.put(product, 0);
        }
        itemsWithQuantities.put(product, itemsWithQuantities.get(product) + amount);
    }

    public void removeItem(ShopProduct product) {
        itemsWithQuantities.remove(product);
    }

    public Integer calculateValue() {
        //TODO
        return 0;
    }

    public Order order(CustomerAddress address) {
        return new Order(itemsWithQuantities, address);
    }

}
