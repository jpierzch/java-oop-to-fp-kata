package com.bari.oopfpkata;

import java.util.HashMap;
import java.util.Map;

public class Order {

    private Map<ShopProduct, Integer> itemsWithAmount = new HashMap<>();

    private CustomerAddress customerAddress;


    public Order(Map<ShopProduct, Integer> itemsWithAmount, CustomerAddress customerAddress) {
        this.itemsWithAmount = itemsWithAmount;
        this.customerAddress = customerAddress;
    }

    public void submit(ProductQuantityUpdatePort productCatalogPort) {
        System.out.println("Order submitted.");
        itemsWithAmount.entrySet().forEach(productWithQuantity ->
                productCatalogPort.decreaseProductQuantities(productWithQuantity.getKey(), productWithQuantity.getValue()));
    }
}
