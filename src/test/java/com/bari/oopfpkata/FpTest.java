package com.bari.oopfpkata;

import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FpTest {

    <A,B,C,D> Function<A, Function<B, Function<C, Function<D, String>>>> f() {
        return a -> b -> c -> d -> String.format("%s, %s, %s, %s", a, b, c, d);
    }

    @Test
    public void test() {
        assertEquals("a, b, c, d", f().apply("a").apply("b").apply("c").apply("d"));
    }
}
